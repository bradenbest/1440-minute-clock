# 1440-minute clock

This is one of the more *literal* examples of a silly pointless weekend project. I discovered that there are no 1440-minute clocks, so I did the logical thing and set out to make one myself. Just because.

This is probably the world's first 1440-minute clock, and for good reason, because in practice, it's not very easy to read.

Maybe one day, I will take it one step further and create the world's first 86400-second clock, where then entire day is measured in-oh wait. They already did that. It's called UNIX Time. Except UT goes for 4294967296 seconds which is about 70 years (2038-1970 = 68).

Okay.
