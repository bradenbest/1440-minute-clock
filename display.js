function Display(args){
    /* Object args
     *     HTMLElement container
     *     Object[] buttons
     *         String label
     *         String action
     */
    var container,
        text;

    function textDisplay()
    {//{{{
        var el = document.createElement("div");

        el.className = "display";
        return el;
    }//}}}

    function mkbutton(button)
    {//{{{
        var el = document.createElement("input");

        el.type = "button";
        el.value = button.label;
        el.onclick = new Function(button.action);

        container.appendChild(el);
    }//}}}

    function mkbreak()
    {//{{{
        return document.createElement("br");
    }//}}}

    function setupButtons(buttons)
    {//{{{
        if(!buttons)
            return;

        buttons.map(mkbutton);
    }//}}}

    function clear()
    {//{{{
        text.innerHTML = "";
    }//}}}

    function print(msg)
    {//{{{
        text.innerHTML += msg;
    }//}}}

    function printf(_format, _arg1, _arg2, etc)
    {//{{{
        var args = Array.from(arguments),
            format = args.shift(),
            parts = format
                .replace(/ /g, "&nbsp;")
                .replace(/\n/g, "<br\>")
                .split("%%"),
            i;

        for(i = 0; i < parts.length; i++){
            print(parts[i]);

            if(args[i] !== undefined){
                if(typeof(args[i]) == "number" && args[i] < 10)
                    print("0");

                print(args[i]);
            }
        }
    }//}}}

    function Display()
    {//{{{
        container = util.getArg(args, "container") || document.body;
        text = textDisplay();
        container.innerHTML = "";
        container.appendChild(text);
        container.appendChild(mkbreak());
        setupButtons(util.getArg(args, "buttons"));

        return {
            print: print,
            printf: printf,
            clear: clear
        };
    }//}}}

    return Display();
}
