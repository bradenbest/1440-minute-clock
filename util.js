var util = (function(){
    function getArg(args, arg)
    {//{{{
        if(args === undefined || args[arg] === undefined)
            return null;

        return args[arg];
    }//}}}

    function synchronize(func, precision)
    {//{{{
        var time = (new Date()).getSeconds(),
            rate = 1000 / (precision % 1000 + 1),
            interval = setInterval(callback(func, time), rate);

        function callback(func, time){
            return function(){
                var time_2 = (new Date()).getSeconds();

                if(time_2 > time){
                    clearInterval(interval);
                    func();
                }
            }
        }
    }//}}}

    function namespace_util()
    {//{{{
        return {
            getArg: getArg,
            synchronize: synchronize
        };
    }//}}}

    return namespace_util();
})();
