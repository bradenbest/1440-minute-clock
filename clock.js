function Clock(args){
    /* Object args
     *     Number fps
     *     Display display
     */
    var display,
        interval,
        fps,
        tick,
        running;

    function getTime()
    {//{{{
        var d = new Date();

        return {
            "hours":   d.getHours(),
            "minutes": d.getMinutes(),
            "seconds": d.getSeconds()
        };
    }//}}}

    function run()
    {//{{{
        var curtime = getTime(),
            fmt_24h = "%%:%%:%%\n",
            fmt_1440m = "%%.%%\n",
            fps_temp = fps;

        if(fps_temp % 2 == 1)
            fps_temp++;

        if(tick++ % fps_temp < fps_temp / 2){
            fmt_24h = fmt_24h.replace(/:/g, " ");
            fmt_1440m = fmt_1440m.replace(/\./g, " ");
        }

        display.clear();
        display.printf("1440m: " + fmt_1440m, curtime.hours * 60 + curtime.minutes, curtime.seconds);
        display.printf("24h:   " + fmt_24h, curtime.hours, curtime.minutes, curtime.seconds);
    }//}}}

    function start()
    {//{{{
        var rate = 1000 / fps;

        if(running)
            return;

        running = true;
        interval = setInterval(run, rate);
    }//}}}

    function stop()
    {//{{{
        if(!running)
            return;

        running = false;
        clearInterval(interval);
    }//}}}

    function Clock()
    {//{{{
        fps = util.getArg(args, "fps") || 1;
        display = util.getArg(args, "display") || new Display();
        tick = 0;
        running = false;

        return {
            start: start,
            stop: stop
        };
    }//}}}

    return Clock();
}
